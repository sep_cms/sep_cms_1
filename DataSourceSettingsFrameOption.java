/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package frameOptionClases;

import gui.MainFrame;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import systemControlClasses.DataSource;

/**
 *
 * @author Administrator
 */
public class DataSourceSettingsFrameOption {
MainFrame mob;
    public DataSourceSettingsFrameOption(MainFrame mf) {
        mob=mf;
    }

    public boolean saveLocation(String path){
        DataSource ds=new DataSource(path);
        try {
            FileOutputStream fos = new FileOutputStream(new File("src/classObjects/dsSetting.cms"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(ds);
            mob.dsLocation=path;
            return true;

        } catch (Exception ex) {
            System.out.println(ex + " -DataSourceSettingsFrameOption");
        }
        return false;
    }

}
