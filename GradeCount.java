/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frameOptionClases;

/**
 *
 * @author toshiba
 */
public class GradeCount {

    private int gradeCount;
    private String grade;
    private int pfCount;
    private String pf;
    private double passRate;
    private String batch;

    public GradeCount(String grade, int gradeCount, String pf, int pfCount) {
        this.gradeCount = gradeCount;
        this.grade = grade;
        this.pfCount = pfCount;
        this.pf = pf;
    }

    public GradeCount(String grade, int gradeCount) {
        this.gradeCount = gradeCount;
        this.grade = grade;
    }

    public GradeCount(int pfCount, String pf) {
        this.pfCount = pfCount;
        this.pf = pf;
    }

    public GradeCount(String batch, double passRate) {
        this.passRate = passRate;
        this.batch = batch;
    }

    public double getPassRate() {
        return passRate;
    }

    public void setPassRate(double passRate) {
        this.passRate = passRate;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public int getGradeCount() {
        return gradeCount;
    }

    public void setGradeCount(int gradeCount) {
        this.gradeCount = gradeCount;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public int getPfCount() {
        return pfCount;
    }

    public void setPfCount(int pfCount) {
        this.pfCount = pfCount;
    }

    public String getPf() {
        return pf;
    }

    public void setPf(String pf) {
        this.pf = pf;
    }
}
