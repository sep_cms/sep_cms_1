/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package frameOptionClases;

import connection.DB;
import gui.MainFrame;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import systemControlClasses.ConnectionSetting;

/**
 *
 * @author Administrator
 */
public class ConnectionSettingFrameOption {
MainFrame mob;

    public ConnectionSettingFrameOption(MainFrame mob) {
        this.mob = mob;
    }

    public boolean tryConnection(String url,String uName,String pWord,String port){
        mob.ipaddress=url;
        mob.username=uName;
        mob.password=pWord;
        mob.port=port;
        if(new DB().con()!=null){
            return saveConnection(url, uName, pWord, port);
        }
        return false;
    }
    public boolean saveConnection(String url,String uName,String pWord,String port){
       ConnectionSetting cs=new ConnectionSetting(url, uName, pWord, port);
        try {
            FileOutputStream fos = new FileOutputStream(new File("src/classObjects/cSetting.cms"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(cs);
            return true;

        } catch (Exception ex) {
            System.out.println(ex + " -ConnectionSettingFrameOPtion");
        }
        return false;
    }

}
