/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frameOptionClases;

import java.io.File;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import systemControlClasses.ExcelReader;
import systemControlClasses.XMLWriter;

/**
 *
 * @author MalithMac
 */
public class ImportStudentDataFrameOption {

    public boolean validateDataSheet(String path) {
        String data[] = new ExcelReader().showExeclStuHeadereDetails(path);
        System.out.println(data[0]+"   "+data[1]);
        if (data[0].equalsIgnoreCase("RegistrationNo") && data[1].equalsIgnoreCase("Name")) {
            return true;
        }
        return false;
    }

    public boolean savaData(String path, String rootPath, JProgressBar sbar, JTable sTable, JLabel statust) {

        Vector dataVec = new ExcelReader().addStuExeclData(path);
        String pathdata[] = new ExcelReader().showExeclStuFileDetails(path);
        if (checkProRata(path)) {
            System.out.println("AAAA");
            pathdata[1]=pathdata[1]+"\\Pro rata";
            for(int i=0;i<pathdata.length;i++){
                System.out.print("xxxx"+pathdata[i]+"\\");
            }
             try {
                boolean result = new XMLWriter(rootPath).createStuDetailsXmlNew(dataVec, pathdata, sbar, sTable, statust,true);
                if (result == true) {
                    // JOptionPane.showMessageDialog(rootPane, "XML successfully created");
                    // excelFileTxt.setText("");
                }
            } catch (Exception ex) {
            }
        } else {
            System.out.println("BBBB1111111111");
            for(int i=0;i<pathdata.length;i++){
                System.out.print(pathdata[i]+"\\");
            }
            System.out.println("BBBB2222222222");
            pathdata = getFileData(pathdata, path);
            for(int i=0;i<pathdata.length;i++){
                System.out.print(pathdata[i]+"\\");
            }
            try {
                boolean result = new XMLWriter(rootPath).createStuDetailsXmlNew(dataVec, pathdata, sbar, sTable, statust,false);
                if (result == true) {
                    // JOptionPane.showMessageDialog(rootPane, "XML successfully created");
                    // excelFileTxt.setText("");
                }
            } catch (Exception ex) {
            }
        }

        return true;
    }

    String[] getFileData(String data[], String path) {
        File f = new File(path);
        data[3] = f.getName().substring(0, 8);
        data[5] = f.getName().substring(9, 16);
        return data;
    }

    boolean checkProRata(String path) {
        //System.out.println(new File(path).getName().substring(0, 8));
        return new File(path).getName().substring(0, 8).equalsIgnoreCase("Pro rata");
        //System.out.println("WWW"+path.substring(path.lastIndexOf("\\"), path.lastIndexOf("."))); 
        //return path.substring(path.lastIndexOf("\\"), path.lastIndexOf(".")).equalsIgnoreCase("Pro rata");


    }
}
